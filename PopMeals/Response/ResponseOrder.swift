//
//  ResponseOrder.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import Foundation

struct ResponseOrder: Codable {
    let orders: [OrderDetail]
    
    enum CodingKeys: String, CodingKey {
        case orders = "orders"
    }
    
    enum Response: Decodable {
        
        enum DecodingError: Error {
            case wrongJSON
        }
        
        case orders(OrderDetail)
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            switch container.allKeys.first {
            case .orders:
                let value = try container.decode(OrderDetail.self, forKey: .orders)
                self = .orders(value)
            default:
                throw DecodingError.wrongJSON
            }
        }
    }
}

struct OrderDetail: Codable {
    let orderId: Int
    let orderStatus: Int?
    let paymentType: String
    
    enum CodingKeys: String, CodingKey {
        case orderId = "order_id"
        case orderStatus = "arrives_at_utc"
        case paymentType = "paid_with"
    }
}
