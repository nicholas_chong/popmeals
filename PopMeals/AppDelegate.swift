//
//  AppDelegate.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        initLaunchPage()
        
        return true
    }

}

// MARK: - AppDelegate
extension AppDelegate {
    func initLaunchPage() {
        window = UIWindow(frame: UIScreen.main.bounds)
        let vc = AccountViewController()
        self.window?.rootViewController = UINavigationController(rootViewController: vc)
        self.window?.makeKeyAndVisible()
    }
}

