//
//  BaseViewController.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        setupView()
    }
    
    func setupView() {
        let button = UIButton()
        let frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        button.setImage(UIImage(named: "icn_back"), for: UIControl.State())
        button.frame = frame
        button.widthAnchor.constraint(equalToConstant: frame.width).isActive = true
        button.heightAnchor.constraint(equalToConstant: frame.height).isActive = true
        button.addTarget(self, action: #selector(popNavigationController), for: .touchUpInside)
        
        navigationItem.hidesBackButton = true
        
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.setLeftBarButtonItems([barButton], animated: true)
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
    }
    
    @objc func popNavigationController() {
        if navigationController?.children.count != 1 {
            navigationController?.popViewController(animated: true)
        }
    }
    
}
