//
//  String+Extension.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import Foundation

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
}
