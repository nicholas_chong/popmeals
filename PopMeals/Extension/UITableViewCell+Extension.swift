//
//  UITableViewCell+Extension.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import UIKit

extension UITableViewCell {
    class var identifier: String { return String.className(self) }
    
    static func getNib() -> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}
