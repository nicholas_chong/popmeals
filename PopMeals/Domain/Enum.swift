//
//  Enum.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import Foundation

enum OrderStatus: String {
    case delivered = "Delivered"
    case cancelled = "Cancelled"
    case confirmed = "Confirmed"
}
