//
//  DataService.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import Foundation
import Alamofire

struct DataService {
    
    static let shared = DataService()
    
    private let endpointUrl = "http://staging-api.dahmakan.com"
    
    private let odersPath = "/test/orders"
    
    
    func getOrderDetails(_ completion: @escaping ((ResponseOrder?, ResponseError?)->Void)) {
        
        let stringUrl = endpointUrl + odersPath
        
        AF.request(stringUrl,
                   method: .get)
            .validate()
            .responseDecodable(of: ResponseOrder.self) { (response) in
                switch response.result {
                case let .success(value):
                    if value.orders.isEmpty {
                        completion(nil, ResponseError(erorMessage: "No orders available."))
                    } else {
                        completion(value, nil)
                    }
                case .failure(_):
                    completion(nil, ResponseError(erorMessage: "Oh no! What happened? :( Make sure the App can handle this temporary error response gracefully."))
                    #if DEBUG
                    print(response.error ?? "")
                    #endif
                }
            }
        
    }
    
}
