//
//  Utilities.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import Foundation

func convertDateFrom(timeStamp: Int,
                     localDate: Bool = true,
                     _ dateFormat: String = "dd MMM yyyy, h:mm aa") -> String {
    let truncatedTime = timeStamp / 1000
    let date = Date(timeIntervalSince1970: TimeInterval(truncatedTime))
    let dateFormatter = DateFormatter()
    if !localDate {
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    }
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.amSymbol = "AM"
    dateFormatter.pmSymbol = "PM"
    dateFormatter.dateFormat = dateFormat
    return dateFormatter.string(from: date)
}
