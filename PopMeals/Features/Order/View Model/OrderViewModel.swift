//
//  OrderViewModel.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import Foundation

class OrderViewModel {
    
    var error: ResponseError? {
        didSet {
            self.showErrorMessage?()
        }
    }
    
    var isLoading: Bool = true {
        didSet {
            self.updateLoadingIndicator?()
        }
    }
    
    var orderDetails: [OrderDetail]? {
        didSet {
            self.didFinishFetch?()
        }
    }
    
    private var dataService: DataService?
    
    var updateLoadingIndicator: (() -> Void)?
    var showErrorMessage: (() -> Void)?
    var didFinishFetch: (() -> Void)?
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func getOrderDetails() {
        self.isLoading = true
        self.dataService?.getOrderDetails({ [weak self] (result, error) in
            guard let self = self else { return }
            self.isLoading = false
            
            if let error = error {
                self.error = error
                return
            }
        
            self.orderDetails = result?.orders.filter {($0.orderStatus != nil)}.sorted(by: { $0.orderStatus ?? 0 > $1.orderStatus ?? 0 })
        })
    }
    
}
