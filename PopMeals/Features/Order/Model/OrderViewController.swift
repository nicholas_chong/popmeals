//
//  OrderViewController.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import UIKit

class OrderViewController: BaseViewController {

    var details: OrderDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Order #" + String(details.orderId)
    }
    
}
