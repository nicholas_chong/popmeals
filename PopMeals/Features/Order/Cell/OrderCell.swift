//
//  OrderCell.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import UIKit

class OrderCell: UICollectionViewCell {
    
    @IBOutlet weak var labelOrderId: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelPaymentMethod: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    
    var viewModel: OrderDetail? {
        didSet {
            labelOrderId.text = "#" + String(viewModel?.orderId ?? 87487324)
            labelTime.text = convertDateFrom(timeStamp: viewModel?.orderStatus ?? 0)
            labelPaymentMethod.text = viewModel?.paymentType
            labelStatus.text = getStatus().rawValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func getStatus() -> OrderStatus {
        let timeStamp = viewModel?.orderStatus ?? 0
        let truncatedTime = timeStamp / 1000
        let date = Date(timeIntervalSince1970: TimeInterval(truncatedTime))
        
        if Date().compare(date) == .orderedDescending || Date().compare(date) == .orderedSame {
            return .delivered
        } else if Date().compare(date) == .orderedAscending {
            return .confirmed
        } else {
            // not available since filtered out
            return .cancelled
        }
    }

}

