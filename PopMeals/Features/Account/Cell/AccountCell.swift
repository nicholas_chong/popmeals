//
//  AccountCell.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import UIKit

class AccountCell: UITableViewCell {

    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var viewModel: AccountModel? {
        didSet {
            imageIcon.image = viewModel?.icon
            labelTitle.text = viewModel?.title.rawValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
