//
//  AccountModel.swift
//  PopMeals
//
//  Created by Nicholas Chong on 26/09/2021.
//

import Foundation
import UIKit

enum AccountTitle: String {
    case profile = "My Profile"
    case address = "My Addresses"
}

struct AccountModel {
    var title: AccountTitle
    var icon: UIImage
}

class Account {
    
    static func getAccountList() -> [AccountModel] {

        let cat1 = AccountModel(title: .profile, icon: UIImage(named: "icn_profile")!)
        let cat2 = AccountModel(title: .address, icon: UIImage(named: "icn_address")!)
        
        return [cat1, cat2]
    }
    
}
